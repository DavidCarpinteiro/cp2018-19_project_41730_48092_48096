#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <zconf.h>
#include "unit.h"
#include "debug.h"

#define TYPE double

static long long wall_clock_time(void) {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (long long int) (tv.tv_sec * 1e6 + tv.tv_usec);
}

int main(int argc, char* argv[]) {
    long i, N;
    long long start, end;
    int c;
    int locPat = -1;//-1;

    // Set size of worker job intensity
    nWorkersFarm = 32;
    farmJobSize = 1000000;
    reduceJobSize = 1000000;
    mapJobSize = 1000000;
    scanJobSize = 1000000;
    pipelineJobSize = 1000000;

    while ((c = getopt (argc, argv, "ds")) != -1) {
        switch (c) {
            case 'd':
                debug = 1;
                break;
            case 's':
                printf("!!!RUNNING SEQUENTIAL!!!\n");
                RUN_PARALLEL = 0;
                break;
            default:
                printf("Invalid option\n");
                abort();
        }
    }

    argc -= optind;
    argv += optind;

    if (argc < 1) {
        printf("Usage: ./example N\n");
        return -1;
    }

    srand((unsigned int) time(NULL));
    srand48(time(NULL));

    N = strtol(argv[0], &argv[0], 0);

    //printf ("Initializing SRC array\n");
    TYPE *src = malloc (sizeof(TYPE) * N);
    if (!src) {
        perror("allocating src"); exit(EXIT_FAILURE);
    }

    for (i = 0; i < N; i++) {
        src[i] = drand48();
    }
    //printf ("Done!\n");
    
    //printDouble (src, N, "SRC");
    if (debug)
        printf ("\n\n");

    if(locPat != -1) {
        start = wall_clock_time();
        testFunction[locPat] (src, N, sizeof(*src));
        end = wall_clock_time();
        printf ("%.2lf\t", (double) (end-start) / 1000 );
        printf("\n");
        RUN_PARALLEL = 0;
        start = wall_clock_time();
        testFunction[locPat] (src, N, sizeof(*src));
        end = wall_clock_time();
        printf ("%.2lf\t", (double) (end-start) / 1000 );

    } else {
        for (int i = 0;  i < nTestFunction;  i++) {
            start = wall_clock_time();
            testFunction[i] (src, N, sizeof(*src));
            end = wall_clock_time();
            printf ("%s:\t%.2lf\tmilliseconds\n", testNames[i], (double) (end-start) / 1000);
            //printf ("%.2lf\t", (double) (end-start) / 1000 );

            if (debug)
                printf ("\n\n");
        }
    }

    printf ("\n");
    free(src);

    return 0;
}
