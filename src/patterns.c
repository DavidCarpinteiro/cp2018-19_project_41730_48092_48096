#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "patterns.h"
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include <sys/time.h>

#define FREE 1
#define BUSY 0

static size_t prevPowerTwo(size_t n) {
    //If its a power of two return the value
    if ((n && !(n & (n - 1))) == 1) {
        return n;//(n >> 1);
    }
    //else find prev
    while (n & (n - 1)) {
        n = n & (n - 1);
    }
    //printf("Power of 2: %zu\n", n);
    return n;
}

void map (void *dest, void *src, size_t nJob, size_t sizeJob, void (*worker)(void *v1, const void *v2)) {
    assert (dest != NULL);
    assert (src != NULL);
    assert (worker != NULL);
    cilk_for (int i=0; i < nJob; i++) {
        worker(dest + i * sizeJob, src + i * sizeJob);
    }
}

void* reduceBinTree(void *src, size_t sizeJob, void (*worker)(void *v1, const void *v2, const void *v3), int low, int high, size_t threshold) {
    if((high-low) <= threshold){
        void *result = malloc(sizeJob);
        memcpy(result, src + low * sizeJob, sizeJob);

        for (int i = low + 1; i<= high; i++) {
            worker(result, result, src + i * sizeJob);
        }
        return result;
    }

    int mid = (low + high) / 2;
    void* left = cilk_spawn reduceBinTree(src, sizeJob, worker, low, mid, threshold);
    void* right = reduceBinTree(src, sizeJob, worker, mid+1, high, threshold);

    cilk_sync;
    
    void* final_result = malloc(sizeJob);
    worker(final_result, left, right);

    return final_result;
}

void reduce (void *dest, void *src, size_t nJob, size_t sizeJob, void (*worker)(void *v1, const void *v2, const void *v3)) {
    assert (dest != NULL);
    assert (src != NULL);
    assert (worker != NULL);

    // Calculate Threshold based on number of Cores
    size_t threshold_b = nJob / (__cilkrts_get_nworkers() * 8);

    void* result = reduceBinTree(src, sizeJob, worker, 0, (int) nJob, threshold_b);
    memcpy(dest, result, sizeJob);
}

// Generic Node for the Scan Pattern implementation
struct NodeG {
    int range_start;            // low limit of the interval
    int range_end;              // high limit of the interval
    void* sum;                  // sum value
    void* fromLeft;             // fromLeft value
    struct NodeG* leftNode;     // right child
    struct NodeG* rightNode;    // left child
};

/*
 * Prefix Sum Algorithm - Up Pass: builds the binary tree
 * */
struct NodeG* upPass (void* src, int low, int high, size_t sizeJob, void (*worker)(void *v1, const void *v2, const void *v3)) {
    // leaf reached
    if(low + 1 == high) {
        struct NodeG* first_leaf = malloc(sizeof(struct NodeG));

        first_leaf->range_start = low;
        first_leaf->range_end = high;
        first_leaf->sum = src + low*sizeJob;

        return first_leaf;
    } else {
        int middle = (low + high) / 2;
        struct NodeG* left = cilk_spawn upPass(src, low, middle, sizeJob, worker);
        struct NodeG* right = upPass(src, middle, high, sizeJob, worker);

        cilk_sync;

        // Father Node
        struct NodeG* father = malloc(sizeof(struct NodeG));
        father->range_start = low;
        father->range_end = high;

        void* sum = malloc(sizeJob);
        worker(sum, left->sum, right->sum);

        father->sum = sum;
        father->leftNode = left;
        father->rightNode = right;

        return father;
    }
}

/*
 * Prefix Sum Algorithm - Down Pass: uses the binary tree to compute the prefix sums
 * */
void downPass (void *dest, void* src, struct NodeG* root, size_t sizeJob, void (*worker)(void *v1, const void *v2, const void *v3)) {
    int diff = root->range_end - root->range_start;

    // when a leaf is reached
    if(diff == 1) {
        if(root->fromLeft == NULL) {
            // Neutral element: the right node takes the fromLeft value of its father
            memcpy(dest + root->range_start*sizeJob, src + root->range_start*sizeJob, sizeJob);
        }else {
            worker(dest + root->range_start * sizeJob, root->fromLeft, src + root->range_start * sizeJob);
        }
    } else {
        struct NodeG* left_child = root->leftNode;
        struct NodeG* right_child = root->rightNode;

        left_child->fromLeft = root->fromLeft;

        // Neutral element
        if (root->fromLeft == NULL) {
            right_child->fromLeft = left_child->sum;
        } else {
            void* fromLeft = malloc(sizeJob);
            worker(fromLeft, root->fromLeft, left_child->sum);
            right_child->fromLeft = fromLeft;
        }

        cilk_spawn downPass(dest, src, left_child, sizeJob, worker);
        downPass(dest, src, right_child, sizeJob, worker);
    }
}

/*
 * Parallel Scan Implementation: uses the Prefix Sum Algorithm
 */
void scan (void *dest, void *src, size_t nJob, size_t sizeJob, void (*worker)(void *v1, const void *v2, const void *v3)) {
    assert (dest != NULL);
    assert (src != NULL);
    assert (worker != NULL);

    //Get a power of 2 or keep value
    nJob = prevPowerTwo(nJob);

    struct NodeG* root = upPass(src, 0, nJob, sizeJob, worker);
    root->fromLeft = NULL;

    downPass(dest, src, root, sizeJob, worker);
}

void prefixUP(const int bits[], int low, int high, int *tree, int pos) {
    if(low + 1 == high) {
        *(tree + pos) = bits[low];
        return;
    }

    int middle = (low + high) >> 1;
    int left_pos = (pos << 1) + 1;
    int right_pos = (pos << 1) + 3;

    cilk_spawn prefixUP(bits, low, middle, tree, left_pos);
    prefixUP(bits, middle, high, tree, right_pos);

    cilk_sync;

    *(tree + pos) = *(tree + left_pos) + *(tree + right_pos);
}

void prefixDOWN(int *tree, int pos, int bitsum[], const int bits[], size_t nJob) {
    long diff = pos - ((nJob << 1) - 1);

    if (diff >= 0) {
        bitsum[diff >> 1] = *(tree + pos + 1) + bits[diff >> 1];
    } else {
        int left_pos = (pos << 1) + 1;
        int right_pos = (pos << 1) + 3;
        *(tree + left_pos + 1) = *(tree + pos + 1);
        *(tree + right_pos + 1) = *(tree + pos + 1) + *(tree + left_pos);

        cilk_spawn prefixDOWN(tree, left_pos, bitsum, bits, nJob);
        prefixDOWN(tree, right_pos, bitsum, bits, nJob);

        //cilk_sync;
    }

}

int pack (void *dest, void *src, size_t nJob, size_t sizeJob, const int *filter) {
    /*
     *  Balanced Binary Tree is store in a array where:
     *      root = 1
     *      each element has 2 ints
     *      for a element at index i:
     *          i -> sum
     *          i + 1 -> fromLeft
     */

    assert(dest != NULL);
    assert(src != NULL);

    //Get a power of 2 or keep value
    nJob = prevPowerTwo(nJob);

    int *tree = malloc(nJob * 4 * sizeof(int));
    int *bit_sum = malloc(nJob * sizeof(int));

    assert(tree != NULL);
    assert(bit_sum != NULL);

    prefixUP(filter, 0, (int) nJob, tree, 1);

    *(tree + 2) = 0;

    prefixDOWN(tree, 1, bit_sum, filter, nJob);

    // Parallel Map
    cilk_for(int i = 0; i < nJob; i++) {
        if (filter[i] == 1) {
            memcpy(dest + (bit_sum[i] - 1) * sizeJob, src + i * sizeJob, sizeJob);
        }
    }

    free(tree);

    int tmp = bit_sum[nJob - 1];

    free(bit_sum);

    return tmp;
}

void gather (void *dest, void *src, size_t nJob, size_t sizeJob, const int *filter, int nFilter) {
    assert (dest != NULL);
    assert (src != NULL);

    // Embarrassing Parallel
    cilk_for (int i=0; i < nFilter; i++) {
        memcpy (dest + i * sizeJob, src + filter[i] * sizeJob, sizeJob);
    }
}

void scatter (void *dest, void *src, size_t nJob, size_t sizeJob, const int *filter) {
    cilk_for (int i=0; i < nJob; i++) {
        memcpy (dest + filter[i] * sizeJob, src + i * sizeJob, sizeJob);
    }
}

void workFirst(void *dest, void *src, size_t sizeJob, size_t nJob, size_t nWorkers,
          void (*workerList[])(void *v1, const void *v2), volatile int *jobs, volatile int *busy, int workerID) {
    int curr_job = 0;
    int nextWorker = workerID + 1;

    // While not all jobs are completed
    while (curr_job < nJob) {
        // Execute job
        workerList[workerID](dest + curr_job * sizeJob, src + curr_job * sizeJob);
        //printf("Worker %d did job: %d\n", workerID, curr_job);fflush(stdout);

        // Wait for next worker to become free
        while (busy[nextWorker] == 1) {
             //printf("Waiting for worker: %d\n", workerID+1);fflush(stdout);
        };
        // Give worker his job
        jobs[nextWorker] = curr_job ;
        //printf("Worker %d sent job: %d\n", workerID, curr_job);fflush(stdout);
        curr_job++;
    }
}

void workLast(void *dest, void *src, size_t sizeJob, size_t nJob, size_t nWorkers,
               void (*workerList[])(void *v1, const void *v2), volatile int *jobs, volatile int *busy, int workerID) {
    int curr_job;

    // Wait for previous worker to give job
    while (jobs[workerID] == -1) {
         //printf("Waiting job1: %d\n", workerID);fflush(stdout);
    }
    // Set flag has busy
    busy[workerID] = 1;
    // Get next job
    curr_job = 0;//jobs[workerID];
     //printf("SWorker %d got job: %d\n", workerID, curr_job);fflush(stdout);


    // While not all jobs are completed
    while (curr_job < nJob) {
        // Execute job
        workerList[workerID](dest + curr_job * sizeJob, dest + curr_job * sizeJob);
        //printf("Worker %d did job: %d\n", workerID, curr_job);fflush(stdout);
        if(curr_job == nJob-1) break;

        // Set flag as free
        busy[workerID] = 0;
        // Wait for job
        while (jobs[workerID] == curr_job) {
           // printf("Waiting job2: %d, lastJob: %d\n", workerID, curr_job);fflush(stdout);
        };
        // Set flag has busy
        busy[workerID] = 1;
        // Get next job
        curr_job++;// = jobs[workerID];
       //printf("EWorker %d got job: %d\n", workerID, curr_job);fflush(stdout);
    }
}

void work(void *dest, void *src, size_t sizeJob, size_t nJob, size_t nWorkers,
          void (*workerList[])(void *v1, const void *v2), volatile int *jobs, volatile int *busy, int workerID) {
    int curr_job;
    int nextWorker = workerID + 1;

    // Wait for previous worker to give job
    while (jobs[workerID] == -1) {
      //  printf("Waiting job1: %d\n", workerID);fflush(stdout);
    }
    // Set flag has busy
    busy[workerID] = 1;
    // Get next job
    curr_job = 0;//jobs[workerID];
   //printf("SWorker %d got job: %d\n", workerID, curr_job);fflush(stdout);


    // While not all jobs are completed
    while (curr_job < nJob) {
        // Execute job
        workerList[workerID](dest + curr_job * sizeJob, dest + curr_job * sizeJob);
        //printf("Worker %d did job: %d\n", workerID, curr_job);fflush(stdout);
        // Wait for next worker to become free
        while (busy[nextWorker] == 1) {
            // printf("Waiting for worker: %d\n", workerID+1);fflush(stdout);
        };
        // Give worker his job
        jobs[nextWorker] = curr_job ;
        //printf("Worker %d sent job: %d\n", workerID, curr_job);fflush(stdout);

        if(curr_job == nJob-1) break;

        // Set flag as free
        busy[workerID] = 0;
        // Wait for job
        while (jobs[workerID] == curr_job) {
            //printf("Waiting job2: %d, lastJob: %d\n", workerID, curr_job);fflush(stdout);
        };
        // Set flag has busy
        busy[workerID] = 1;
        // Get next job
        curr_job++;// = jobs[workerID];
        //printf("EWorker %d got job: %d\n", workerID, curr_job);fflush(stdout);

    }
}

void pipeline(void *dest, void *src, size_t nJob, size_t sizeJob, void (*workerList[])(void *v1, const void *v2),
              size_t nWorkers) {
    //indexes of current jobs
    volatile int job[nWorkers];
    //flag for jobs being actively worked on
    volatile int busy[nWorkers];

    //init arrays
    for (int i = 0; i < nWorkers; i++) {
        busy[i] = 0;
        job[i] = -1;
    }
    if(nWorkers < 2) {
        printf("Need at least 2 workers");
        abort();
    }
    else if(nWorkers == 2) {
        cilk_spawn workFirst(dest, src, sizeJob, nJob, nWorkers, workerList, job, busy, 0);
        cilk_spawn workLast(dest, src, sizeJob, nJob, nWorkers, workerList, job, busy, 1);
    }else {
        cilk_spawn workFirst(dest, src, sizeJob, nJob, nWorkers, workerList, job, busy, 0);
        for (int i = 1; i < nWorkers-1; i++) {
            //spwan workers
            cilk_spawn work(dest, src, sizeJob, nJob, nWorkers, workerList, job, busy, i);
        }
        cilk_spawn workLast(dest, src, sizeJob, nJob, nWorkers, workerList, job, busy, nWorkers-1);
    }

    cilk_sync;
}

void farmJob (void *out, void *in, void (*worker)(void *v1, const void *v2), int *state) {
	worker(out, in);
	*state = FREE;
}

static long long wall_clock_time(void) {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (long long int) (tv.tv_sec * 1e6 + tv.tv_usec);
}

void farm (void *dest, void *src, size_t nJob, size_t sizeJob, void (*worker)(void *v1, const void *v2), size_t nWorkers) {
	assert(dest != NULL);
    assert(src != NULL);
    assert(worker != NULL);

    long count = 0;
	int states[nWorkers];
	int curr_worker = 0;

	int time_interval = 10;
	long long time_start, sleep_time = 0;

	// Set workers state to free
	for(int i = 0; i < nWorkers; i++) {
		states[i] = FREE;
	}

	// While there is input from stream, launch new jobs
	while(count < nJob) {
        time_start = -1;
		// If worker is busy find next free

		int tmp = curr_worker;
        while(states[curr_worker] == BUSY) {
			if(++curr_worker == nWorkers) {
				curr_worker = 0;
			}

			if(tmp == curr_worker) {
                //All are busy
                usleep((__useconds_t) sleep_time);

                if(count % time_interval == 0 && time_start == -1) {
                    time_start = wall_clock_time();
                }
			}
		}

		if(count % time_interval == 0 && time_start != -1) {
            sleep_time = (long long int) (((sleep_time + (wall_clock_time() - time_start) ) / 2) * 0.75);
            //printf("Time: %lli\n", sleep_time);
            //fflush(stdout);
        }

        states[curr_worker] = BUSY;
		cilk_spawn farmJob(dest + count * sizeJob, src + count * sizeJob, worker, &states[curr_worker]);

		count++;
	}

	cilk_sync;
}
