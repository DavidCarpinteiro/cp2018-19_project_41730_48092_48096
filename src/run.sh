#!/bin/bash
rm -f res.out;
start=`date +%s`
for size in 100000 500000 1000000 5000000 10000000 50000000 100000000; do
	echo -e 'Size\t'$size >> res.out;
	for cores in 1 2 4 8 16; do
		echo -e 'Cores\t'$cores >> res.out;
		for tries in 1 2 3 4 5; do
            CILK_NWORKERS=$cores ./main $size  >> res.out;
		done;
		echo >> res.out;
	done
	echo $size "done"
done
end=`date +%s`
runtime=$((end-start))
echo Finished in $runtime's';
#| sed 1,2d | sed -e "s/milliseconds//"