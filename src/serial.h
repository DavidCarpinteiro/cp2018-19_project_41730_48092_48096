#ifndef __SERIAL_H
#define __SERIAL_H

#include <glob.h>

void s_map (
        void *dest,           // Target array
        void *src,            // Source array
        size_t nJob,          // # elements in the source array
        size_t sizeJob,       // Size of each element in the source array
        void (*worker)(void *v1, const void *v2) // [ v1 = op (v2) ]
);

void s_reduce (
        void *dest,           // Target array
        void *src,            // Source array
        size_t nJob,          // # elements in the source array
        size_t sizeJob,       // Size of each element in the source array
        void (*worker)(void *v1, const void *v2, const void *v3) // [ v1 = op (v2, v3) ]
);

void s_scan (
        void *dest,           // Target array
        void *src,            // Source array
        size_t nJob,          // # elements in the source array
        size_t sizeJob,       // Size of each element in the source array
        void (*worker)(void *v1, const void *v2, const void *v3) // [ v1 = op (v2, v3) ]
);

int s_pack (
        void *dest,           // Target array
        void *src,            // Source array
        size_t nJob,          // # elements in the source array
        size_t sizeJob,       // Size of each element in the source array
        const int *filter     // Filer for pack
);

void s_gather (
        void *dest,           // Target array
        void *src,            // Source array
        size_t nJob,          // # elements in the source array
        size_t sizeJob,       // Size of each element in the source array
        const int *filter,    // Filter for gather
        int nFilter           // # elements in the filter
);

void s_scatter (
        void *dest,           // Target array
        void *src,            // Source array
        size_t nJob,          // # elements in the source array
        size_t sizeJob,       // Size of each element in the source array
        const int *filter     // Filter for scatter
);

void s_pipeline (
        void *dest,           // Target array
        void *src,            // Source array
        size_t nJob,          // # elements in the source array
        size_t sizeJob,       // Size of each element in the source array
        void (*workerList[])(void *v1, const void *v2), // one function for each stage of the pipeline
        size_t nWorkers       // # stages in the pipeline
);

void s_farm (
        void *dest,           // Target array
        void *src,            // Source array
        size_t nJob,          // # elements in the source array
        size_t sizeJob,       // Size of each element in the source array
        void (*worker)(void *v1, const void *v2),  // [ v1 = op (22) ]
        size_t nWorkers       // # workers in the farm
);

#endif
