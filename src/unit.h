#ifndef __UNIT_H
#define __UNIT_H

typedef void (*TESTFUNCTION)(void *, size_t, size_t);

extern TESTFUNCTION testFunction[];

extern char *testNames[];

extern int nTestFunction;

extern int RUN_PARALLEL;

extern int scanJobSize;

extern int nWorkersFarm;
extern int farmJobSize;

extern int reduceJobSize;
extern int mapJobSize;
extern int pipelineJobSize;

#endif