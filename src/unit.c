#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include "patterns.h"
#include "serial.h"
#include "debug.h"
#include "unit.h"


#define TYPE double
#define FMT "%lf"

int RUN_PARALLEL = 1;
int nWorkersFarm = 4;
int farmJobSize = 1;
int reduceJobSize = 1;
int mapJobSize = 1;
int scanJobSize = 1;
int pipelineJobSize = 1;

//=======================================================
// Workers
//=======================================================
/*
static void workerMax(void *a, const void *b, const void *c) {
    // a = max (b, c)
    if (*(TYPE *) c > *(TYPE *) b)
        *(TYPE *) a = *(TYPE *) c;
    else
        *(TYPE *) a = *(TYPE *) b;
}

static void workerMin(void *a, const void *b, const void *c) {
    // a = min (b, c)
    *(TYPE *) a = *(TYPE *) b;
    if (*(TYPE *) c < *(TYPE *) a)
        *(TYPE *) a = *(TYPE *) c;
}

static void workerSubtract(void *a, const void *b, const void *c) {
    // a = n - c
    *(TYPE *) a = *(TYPE *) b - *(TYPE *) c;
}


static void workerMultiply(void *a, const void *b, const void *c) {
    // a = b * c
    *(TYPE *) a = *(TYPE *) b + *(TYPE *) c;
}*/

static void workerAddScan(void *a, const void *b, const void *c) {
    // a = b + c
    for (int i = 0; i < scanJobSize; i++) ;
    *(TYPE *) a = *(TYPE *) b + *(TYPE *) c;
}

static void workerAddReduce(void *a, const void *b, const void *c) {
    // More intensive job
    for (int i = 0; i < reduceJobSize; i++) ;
    *(TYPE *) a = *(TYPE *) b + *(TYPE *) c;
}

static void workerAddOne(void *a, const void *b) {
    // a = b + 1
    for (int i = 0; i < pipelineJobSize; i++) ;
    *(TYPE *) a = *(TYPE *) b + 1;
}

static void workerAddOneMap(void *a, const void *b) {
    // a = b + 1
    for (int i = 0; i < mapJobSize; i++) ;
    *(TYPE *) a = *(TYPE *) b + 1;
}

static void workerAddOneFarm(void *a, const void *b) {
    for (int i = 0; i < farmJobSize; i++) ;
    *(TYPE *) a = *(TYPE *) b + 1;
}

static void workerMultTwo(void *a, const void *b) {
    // a = b * 2
    for (int i = 0; i < pipelineJobSize; i++) ;
    *(TYPE *) a = *(TYPE *) b * 2;
}

static void workerDivTwo(void *a, const void *b) {
    // a = b / 2
    for (int i = 0; i < pipelineJobSize; i++) ;
    *(TYPE *) a = *(TYPE *) b / 2;
}


//=======================================================
// Unit testing functions
//=======================================================

void testMap(void *src, size_t n, size_t size) {
    TYPE *dest = malloc(n * size);

    if (RUN_PARALLEL) {
        map(dest, src, n, size, workerAddOneMap);
    } else {
        s_map(dest, src, n, size, workerAddOneMap);
    }

    printDouble(dest, n, __FUNCTION__);
    free(dest);
}

void testReduce(void *src, size_t n, size_t size) {
    TYPE *dest = malloc(size);

    if (RUN_PARALLEL) {
        reduce(dest, src, n, size, workerAddReduce);
    } else {
        s_reduce(dest, src, n, size, workerAddReduce);
    }

    printDouble(dest, 1, __FUNCTION__);
    free(dest);
}

void testScan(void *src, size_t n, size_t size) {

    TYPE *dest = malloc(n * size);

    if (RUN_PARALLEL) {
        scan(dest, src, n, size, workerAddScan);
    } else {
        s_scan(dest, src, n, size, workerAddScan);
    }

    printDouble(dest, n, __FUNCTION__);
    free(dest);
}

void testPack(void *src, size_t n, size_t size) {
    int nFilter = 0;

    int *filter = calloc(n, sizeof(*filter));
    for (int i = 0; i < n; i++) {
        if(i % 2 == 0) {
            nFilter++;
            filter[i] = 1;
        }
    }

    TYPE *dest = malloc(nFilter * size);

    int newN;
    if (RUN_PARALLEL) {
        newN = pack(dest, src, n, size, filter);
    } else {
        newN = s_pack(dest, src, n, size, filter);
    }

    printInt(filter, n, "filter");
    printDouble(dest, newN, __FUNCTION__);
    free(filter);
    free(dest);
}

void testGather(void *src, size_t n, size_t size) {
    int nFilter = (int) n > 1000 ? (int) n / 1000 : (int) n;
    TYPE *dest = malloc(nFilter * size);
    int filter[nFilter];
    for (int i = 0; i < nFilter; i++)
        filter[i] = rand() % (int) n;
    printInt(filter, nFilter, "filter");

    if (RUN_PARALLEL) {
        gather(dest, src, n, size, filter, nFilter);
    } else {
        s_gather(dest, src, n, size, filter, nFilter);
    }

    printDouble(dest, nFilter, __FUNCTION__);
    free(dest);
}

void testScatter(void *src, size_t n, size_t size) {
    int nDest = (int) n > 1000 ? (int) n / 1000 : (int) n;
    TYPE *dest = malloc(nDest * size);
    memset(dest, 0, nDest * size);
    int *filter = calloc(n, sizeof(*filter));
    for (int i = 0; i < n; i++)
        filter[i] = rand() % nDest;
    printInt(filter, n, "filter");

    if (RUN_PARALLEL) {
        scatter(dest, src, n, size, filter);
    } else {
        s_scatter(dest, src, n, size, filter);
    }

    printDouble(dest, nDest, __FUNCTION__);
    free(filter);
    free(dest);
}

void testPipeline(void *src, size_t n, size_t size) {
    void (*pipelineFunction[])(void *, const void *) = {
            workerMultTwo,
            workerAddOne,
            workerDivTwo
    };
    int nPipelineFunction = sizeof(pipelineFunction) / sizeof(pipelineFunction[0]);
    TYPE *dest = malloc(n * size);

    if (RUN_PARALLEL) {
        pipeline(dest, src, n, size, pipelineFunction, nPipelineFunction);
    } else {
        s_pipeline(dest, src, n, size, pipelineFunction, nPipelineFunction);
    }


    printDouble(dest, n, __FUNCTION__);
    free(dest);
}

void testFarm(void *src, size_t n, size_t size) {
    TYPE *dest = malloc(n * size);

    if (RUN_PARALLEL) {
        farm(dest, src, n, size, workerAddOneFarm, nWorkersFarm);
    } else {
        s_farm(dest, src, n, size, workerAddOneFarm, nWorkersFarm);
    }

    printDouble(dest, n, __FUNCTION__);
    free(dest);
}


//=======================================================
// List of unit test functions
//=======================================================


typedef void (*TESTFUNCTION)(void *, size_t, size_t);

TESTFUNCTION testFunction[] = {
        testMap,
        testReduce,
        testScan,
        testPack,
        testGather,
        testScatter,
        testPipeline,
        testFarm,
};

char *testNames[] = {
        "testMap",
        "testReduce",
        "testScan",
        "testPack",
        "testGather",
        "testScatter",
        "testPipeline",
        "testFarm",
};

int nTestFunction = sizeof(testFunction) / sizeof(testFunction[0]);


