# 1. Correr sequencialmente:
	Testar no node9 com:
	git clone/pull ...
	cd src
	chmod +x run.sh
	./run.sh
		

# 2. Implementar Padrões:
## Catarina:
	Scan, Pack, Map

## David:
	Reduce, Gather, Farm

## Andre:
	Scatter, Pipeline
	
# 3. Avaliar Padrões:
	Correr no node9 com os mesmos valores do sequencial
	Registar valores na Drive/resultados
	
# 4. Relatório
	Apresentar e discutir resultados
	

# Para correr no servidor node9:
	Log on to the server with the command:
		ssh g16@10.170.138.240
	The password is literally (we need to change the pass):
		16grupo16!
